// Package main Innius KPI Web API
//
// Web API for innius kpis
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: https
//     BasePath: /
//     Version: 2.0.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: innius<support@innius.com> http://innius.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     SecurityDefinitions:
//       sigv4:
//         type: "apiKey"
//         name: "Authorization"
//         in: "header"
//         x-amazon-apigateway-authtype: "awsSigv4"
// swagger:meta
package main
