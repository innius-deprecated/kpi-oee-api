module bitbucket.org/to-increase/kpi-oee-api

require (
	bitbucket.org/innius/apihttp v1.0.3
	bitbucket.org/to-increase/go-auth v1.0.0
	bitbucket.org/to-increase/go-database-connection v1.0.4
	bitbucket.org/to-increase/go-event-queue v0.0.0-20180705101638-f10daa6e8071
	bitbucket.org/to-increase/go-middleware v1.0.18
	bitbucket.org/to-increase/go-sdk v1.0.14
	bitbucket.org/to-increase/go-sns v1.0.5
	bitbucket.org/to-increase/go-trn v1.0.1
	bitbucket.org/to-increase/vogels v1.0.0
	github.com/akrylysov/algnhsa v0.0.0-20190203201208-70f315bb89bd
	github.com/aws/aws-sdk-go v1.16.34
	github.com/aws/aws-xray-sdk-go v1.0.0-rc.9
	github.com/go-chi/chi v4.0.1+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-redis/redis v6.15.1+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/gommon v0.2.8
	github.com/lib/pq v1.0.0
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
	github.com/vrischmann/envconfig v1.1.0
	golang.org/x/net v0.0.0-20190110200230-915654e7eabc
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
