package calculation

import "bitbucket.org/to-increase/kpi-oee-api/types"

func Status(value float64, thresholds []types.Threshold) int {
	status := 0
	for _, thr := range thresholds {
		if thr.Status > status && value < thr.Value {
			status = thr.Status
		}
	}
	return status
}
