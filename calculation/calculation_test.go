package calculation

import (
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var ctx = context.Background()

func testAggregate(mid string, ts int64) types.TimestampedOEEAggregate {
	return types.TimestampedOEEAggregate{
		Timestamp:     ts,
		TotalProduced: 300,
		OnTime:        600,
		MaxProduced:   600,
		BadProduced:   90,
		ErrorTime:     150,
	}
}

func testAggregates(mid string, start, end int64) []types.TimestampedOEEAggregate {
	aggregates := []types.TimestampedOEEAggregate{}
	for i := start; i <= end; i += 24 * 60 * 60 {
		aggregates = append(aggregates, testAggregate(mid, i))
	}
	return aggregates
}

func testHourlyAggregates(mid string, start, end int64) []types.TimestampedOEEAggregate {
	aggregates := []types.TimestampedOEEAggregate{}
	for i := start; i <= end; i += 60 * 60 {
		aggregates = append(aggregates, testAggregate(mid, i))
	}
	return aggregates
}

func TestCalculation(t *testing.T) {
	query, err := aggregation_repo.NewTestAggregationRepo()
	assert.NoError(t, err)
	cid := uuid.NewV4().String()

	mid := trn.NewMachine(cid).String()

	start := time.Now()
	end := start.AddDate(0, 0, 8)

	for _, a := range testAggregates(mid, start.Unix(), end.Unix()) {
		assert.NoError(t, query.Set(ctx, mid, a))
	}
	for _, a := range testHourlyAggregates(mid, start.Unix(), end.Unix()) {
		assert.NoError(t, query.SetHourly(ctx, mid, a))
	}
	value, err := Calculate(ctx, query, mid, &types.Timeframe{start.Unix(), end.Unix()})
	assert.NoError(t, err)

	expected := types.OeeValue{
		Availability: 80,
		Performance:  50,
		Quality:      70,
		OEE:          28,
	}
	assert.InDelta(t, expected.Availability, value.Availability, 0.01)
	assert.InDelta(t, expected.Performance, value.Performance, 0.01)
	assert.InDelta(t, expected.Quality, value.Quality, 0.01)
	assert.InDelta(t, expected.OEE, value.OEE, 0.01)

	t.Run("Calculate details", func(t *testing.T) {
		values, totals, err := CalculateDetails(ctx, query, mid, &types.Timeframe{start.Unix(), end.AddDate(0, 0, 10).Unix()})
		assert.NoError(t, err)
		assert.Len(t, values, 9)
		for _, value := range values {
			assert.InDelta(t, expected.Availability, value.Availability, 0.01)
			assert.InDelta(t, expected.Performance, value.Performance, 0.01)
			assert.InDelta(t, expected.Quality, value.Quality, 0.01)
			assert.InDelta(t, expected.OEE, value.OEE, 0.01)
		}
		assert.InDelta(t, expected.OEE, totals.OEE, 0.01)
	})

	t.Run("Details of a short period", func(t *testing.T) {
		values, totals, err := CalculateDetails(ctx, query, mid, &types.Timeframe{start.Unix(), start.AddDate(0, 0, 2).Unix()})
		assert.NoError(t, err)
		assert.InDelta(t, 48, len(values), 1)
		for _, value := range values {
			assert.InDelta(t, expected.Availability, value.Availability, 0.01)
			assert.InDelta(t, expected.Performance, value.Performance, 0.01)
			assert.InDelta(t, expected.Quality, value.Quality, 0.01)
			assert.InDelta(t, expected.OEE, value.OEE, 0.01)
		}
		assert.InDelta(t, expected.OEE, totals.OEE, 0.01)
	})
}
