package calculation

import (
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
)

func Calculate(c context.Context, q aggregation_repo.AggregationRepository, machine string, timeframe *types.Timeframe) (types.OeeValue, error) {
	dailyValues, err := q.GetDetails(c, machine, timeframe)
	if err != nil {
		return types.OeeValue{}, err
	}
	return ConvertToValues(reduce(dailyValues)), nil
}

func CalculateShift(c context.Context, q aggregation_repo.AggregationRepository, machine, shift string, timeframe *types.Timeframe) (types.OeeValue, error) {
	dailyValues, err := q.GetShift(c, machine, shift, timeframe)
	if err != nil {
		return types.OeeValue{}, err
	}
	return ConvertToValues(reduce(dailyValues)), nil
}

func CalculateDetails(c context.Context, q aggregation_repo.AggregationRepository, machine string, timeframe *types.Timeframe) ([]types.TimestampedOeeValue, types.OeeValue, error) {
	dailyValues, err := q.GetDetails(c, machine, timeframe)
	if err != nil {
		return nil, types.OeeValue{}, err
	}
	response := []types.TimestampedOeeValue{}
	for _, dailyValue := range dailyValues {
		response = append(response,
			types.TimestampedOeeValue{
				OeeValue:  ConvertToValues(dailyValue),
				Timestamp: dailyValue.Timestamp,
			})
	}
	totals := ConvertToValues(reduce(dailyValues))
	return response, totals, nil
}


func ConvertToValues(in types.TimestampedOEEAggregate) types.OeeValue {
	a, p, q := 0.0, 0.0, 0.0

	if in.OnTime+in.ErrorTime > 0 {
		a = in.OnTime / (in.OnTime + in.ErrorTime)
	}
	if in.MaxProduced > 0 {
		p = in.TotalProduced / in.MaxProduced
	}
	if in.TotalProduced > 0 {
		q = (in.TotalProduced - in.BadProduced) / in.TotalProduced
	}
	return types.OeeValue{
		OEE:          a * p * q * 100,
		Availability: a * 100,
		Performance:  p * 100,
		Quality:      q * 100,

		OnTime:        in.OnTime,
		ErrorTime:     in.ErrorTime,
		TotalProduced: in.TotalProduced,
		MaxProduced:   in.MaxProduced,
		BadProduced:   in.BadProduced,
	}
}

//takes a slice of daily aggregates and sums all properties
func reduce(input []types.TimestampedOEEAggregate) types.TimestampedOEEAggregate {
	result := types.TimestampedOEEAggregate{}
	for _, agg := range input {
		result.OnTime += agg.OnTime
		result.ErrorTime += agg.ErrorTime
		result.TotalProduced += agg.TotalProduced
		result.MaxProduced += agg.MaxProduced
		result.BadProduced += agg.BadProduced
	}
	return result
}
