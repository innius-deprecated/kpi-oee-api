package calculation

import (
	"testing"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"github.com/stretchr/testify/assert"
)

func TestTrend(t *testing.T) {
	current := types.OeeValue{
		OEE: 50,
	}
	t.Run("Trending upwards", func(t *testing.T) {
		previous := types.OeeValue{
			OEE: 40,
		}
		assert.Equal(t, 1, Trend(previous, current))
	})
	t.Run("Trending downwards", func(t *testing.T) {
		previous := types.OeeValue{
			OEE: 60,
		}
		assert.Equal(t, 4, Trend(previous, current))
	})
	t.Run("Trending flat", func(t *testing.T) {
		previous := types.OeeValue{
			OEE: 50,
		}
		assert.Equal(t, 0, Trend(previous, current))
	})
}
