package calculation

import (
	"bitbucket.org/to-increase/kpi-oee-api/types"
)



func Trend(previous, current types.OeeValue) int {
	//	0 - NEUTRAL
	//	1 - UPWARDPOSITIVE
	//	2 - UPWARDNEGATIVE
	//	3 - DOWNWARDPOSITIVE
	//	4 - DOWNWARDNEGATIVE
	if previous.OEE > current.OEE {
		return 4
	}
	if previous.OEE < current.OEE {
		return 1
	}
	return 0
}
