package calculation

import (
	"testing"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"github.com/stretchr/testify/assert"
)

func TestStatus(t *testing.T) {
	thresholds := []types.Threshold{
		{Status: 1, Value: 60},
		{Status: 2, Value: 40},
	}

	assert.Equal(t, 0, Status(90, thresholds))
	assert.Equal(t, 1, Status(50, thresholds))
	assert.Equal(t, 2, Status(30, thresholds))
}
