package aggregation_repo

import (
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"fmt"
	"github.com/go-redis/redis"
	"sort"
)

const redis_prefix = "kpi-oee/"

type AggregationRepository interface {
	Set(c context.Context, machine string, agg ...types.TimestampedOEEAggregate) error
	SetHourly(c context.Context, machine string, agg ...types.TimestampedOEEAggregate) error
	GetDetails(c context.Context, machine string, timeframe *types.Timeframe) ([]types.TimestampedOEEAggregate, error)

	SetShift(c context.Context, machine string, shift string, agg ...types.TimestampedOEEAggregate) error
	GetShift(c context.Context, machine, shift string, timeframe *types.Timeframe) ([]types.TimestampedOEEAggregate, error)

	Delete(c context.Context, machine string) error
}

func newRedisClient(addr string) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewAggregationRepo(addr string) (AggregationRepository, error) {
	client, err := newRedisClient(addr)
	if err != nil {
		return nil, err
	}
	return &query{
		redis: client,
	}, nil
}

func NewTestAggregationRepo() (AggregationRepository, error) {
	return NewAggregationRepo("localhost:6379")
}

type query struct {
	redis *redis.Client
}

func (q *query) GetDetails(c context.Context, machine string, timeframe *types.Timeframe) ([]types.TimestampedOEEAggregate, error) {
	if timeframe.UseDailyAggregation() {
		return q.zRangeByScore(machineKey(machine), timeframe.DateFrom, timeframe.DateTo)
	}
	return q.zRangeByScore(hourlyKey(machine), timeframe.DateFrom, timeframe.DateTo)
}

func (q *query) zRangeByScore(key string, min, max int64) ([]types.TimestampedOEEAggregate, error) {
	results := []types.TimestampedOEEAggregate{}
	err := q.redis.ZRangeByScore(key, redis.ZRangeBy{Min: fmt.Sprintf("%d", min), Max: fmt.Sprintf("%d", max)}).ScanSlice(&results)
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (q *query) Set(c context.Context, machine string, agg ...types.TimestampedOEEAggregate) error {
	return q.zAdd(c, machineKey(machine), agg)
}

func min(agg []types.TimestampedOEEAggregate) int64 {
	sort.Slice(agg, func(i, j int) bool {
		return agg[i].Timestamp > agg[j].Timestamp
	})
	return agg[len(agg)-1].Timestamp
}

func (q *query) zAdd(c context.Context, key string, agg []types.TimestampedOEEAggregate) error {
	if len(agg) == 0 {
		return nil
	}
	if err := q.redis.ZRemRangeByScore(key, fmt.Sprintf("%d", min(agg)), "+inf").Err(); err != nil {
		return err
	}
	members := make([]redis.Z, len(agg))
	for i := range agg {
		members[i] = redis.Z{Member: agg[i], Score: float64(agg[i].Timestamp)}
	}
	return q.redis.ZAdd(key, members...).Err()
}
func (q *query) SetHourly(c context.Context, machine string, agg ...types.TimestampedOEEAggregate) error {
	return q.zAdd(c, hourlyKey(machine), agg)
}

func (q *query) GetShift(c context.Context, machine, shift string, timeframe *types.Timeframe) ([]types.TimestampedOEEAggregate, error) {
	return q.zRangeByScore(shiftKey(machine, shift), timeframe.DateFrom, timeframe.DateTo)
}

func (q *query) SetShift(c context.Context, machine, shift string, agg ...types.TimestampedOEEAggregate) error {
	return q.zAdd(c, shiftKey(machine, shift), agg)
}

func machineKey(machine string) string {
	return redis_prefix + machine
}

func shiftKey(machine, shift string) string {
	return machineKey(machine) + "/shift/" + shift
}

func hourlyKey(machine string) string {
	return machineKey(machine) + "/hourly"
}

func (q *query) Delete(c context.Context, machine string) error {
	keys, err := q.redis.Keys(machineKey(machine) + "*").Result()
	if err != nil {
		return err
	}
	return q.redis.Del(keys...).Err()
}
