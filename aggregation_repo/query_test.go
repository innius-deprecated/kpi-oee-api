package aggregation_repo

import (
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestQueryRepository(t *testing.T) {
	q, err := NewTestAggregationRepo()
	assert.NoError(t, err)

	date := time.Now().Unix()
	mid := trn.NewMachine(uuid.NewV4().String()).String()
	dailyValue := types.TimestampedOEEAggregate{
		Timestamp:     date,
		TotalProduced: 300,
		OnTime:        600,
		MaxProduced:   600,
		BadProduced:   90,
		ErrorTime:     150,
	}
	ctx := context.Background()
	assert.NoError(t, q.Set(ctx, mid, dailyValue))

	retrieved, err := q.GetDetails(ctx, mid, &types.Timeframe{date, date * 2})
	assert.NoError(t, err)
	assert.Len(t, retrieved, 1)
	assert.Equal(t, dailyValue, retrieved[0])

	t.Run("update the aggregate", func(t *testing.T) {
		dailyValue.BadProduced = 601
		assert.NoError(t, q.Set(ctx, mid, dailyValue))
		retrieved, err := q.GetDetails(ctx, mid, &types.Timeframe{date, date * 2})
		assert.NoError(t, err)
		assert.Len(t, retrieved, 1)
		assert.EqualValues(t, 601, dailyValue.BadProduced)
	})

	t.Run("daily and hourly dailyValue", func(t *testing.T) {
		hourlyValue := types.TimestampedOEEAggregate{
			Timestamp:     date,
			TotalProduced: 999,
			OnTime:        600,
			MaxProduced:   600,
			BadProduced:   90,
			ErrorTime:     150,
		}
		assert.NoError(t, q.Set(ctx, mid, hourlyValue))
		retrieved, err := q.GetDetails(ctx, mid, &types.Timeframe{date, date * 2})
		assert.NoError(t, err)
		assert.EqualValues(t, hourlyValue, retrieved[0])
	})

	shiftId := uuid.NewV4().String()
	t.Run("Aggregated data for a shift", func(t *testing.T) {
		assert.NoError(t, q.SetShift(context.Background(), mid, shiftId, dailyValue))

		retrieved, err := q.GetShift(context.Background(), mid, shiftId, &types.Timeframe{date, date + 100})
		assert.NoError(t, err)
		assert.Len(t, retrieved, 1)
	})

	t.Run("Delete data", func(t *testing.T) {
		assert.NoError(t, q.Delete(context.Background(), mid))

		retrieved, err := q.GetDetails(context.Background(), mid, &types.Timeframe{date, date + 100})
		assert.NoError(t, err)
		assert.Empty(t, retrieved)

		retrieved2, err := q.GetShift(context.Background(), mid, shiftId, &types.Timeframe{date, date + 100})
		assert.NoError(t, err)
		assert.Empty(t, retrieved2)
	})
}

func TestDateSelection(t *testing.T) {
	q, err := NewTestAggregationRepo()
	assert.NoError(t, err)

	date := time.Now()
	mid := trn.NewMachine(uuid.NewV4().String()).String()
	for i := 0; i < 5; i++ {
		values := types.TimestampedOEEAggregate{
			Timestamp:     date.AddDate(0, 0, 10*i).Unix(),
			TotalProduced: 300,
			OnTime:        600,
			MaxProduced:   600,
			BadProduced:   90,
			ErrorTime:     150,
		}
		assert.NoError(t, q.Set(context.Background(), mid, values))
	}

	retrieved, err := q.GetDetails(context.Background(), mid, &types.Timeframe{date.AddDate(0, 0, 15).Unix(), date.AddDate(0, 0, 36).Unix()})
	assert.NoError(t, err)
	assert.Len(t, retrieved, 2)
}
