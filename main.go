package main

import (
	"context"
	"github.com/akrylysov/algnhsa"
	"github.com/aws/aws-xray-sdk-go/strategy/ctxmissing"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
)

func main() {
	conf := Config{}
	if err := envconfig.Init(&conf); err != nil {
		panic(err)
	}

	if level, err := logrus.ParseLevel(conf.LogLevel); err == nil {
		logrus.SetLevel(level)
	}
	srv, err := NewServer(conf)
	if err != nil {
		panic(err)
	}
	if err := xray.Configure(xray.Config{
		ContextMissingStrategy: ctxmissing.NewDefaultLogErrorStrategy(),
	}); err != nil {
		logrus.Printf("Can't initialize xray: %v", err)
	}

	srv.routes()
	go srv.machineEventSubscr.Subscribe()

	// TODO: remove this conversion code after deployment to live
	go func() {
		logrus.Print("auto conversion started")
		cfgs, err := srv.repository.GetAllConfigs(context.Background())
		if err != nil {
			panic(err)
		}
		for i := range cfgs {
			if !cfgs[i].Migrated {
				cfgs[i].LastUpdated = 0
				cfgs[i].Migrated = true
				if err := srv.repository.SaveConfig(context.Background(), *cfgs[i]); err != nil {
					panic(err)
				}
			}
		}
		logrus.Print("auto conversion completed")
	}()
	algnhsa.ListenAndServe(srv, nil)
}
