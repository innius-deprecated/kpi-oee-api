package main

import (
	"github.com/stretchr/testify/assert"
	"github.com/vrischmann/envconfig"
	"os"
	"testing"
)

func TestConfig(t *testing.T) {
	os.Setenv("ENVIRONMENT_NAME", "chair")
	os.Setenv("SERVICE_NAME", "kpi-oee-api")
	os.Setenv("REPOSITORY_DYNAMODB_TABLENAME", "table1")

	os.Setenv("BASE_PATH", "/")

	os.Setenv("MACHINE_EVENT_SNS_TOPIC", "topic")
	os.Setenv("EXTERNAL_SERVICE_ENDPOINT", "https://api.test.innius.com/oee")
	os.Setenv("KPI_EVENT_SNS_TOPIC", "topic")
	os.Setenv("REDSHIFT_SECRET_NAME", "secret")

	os.Setenv("AGGREGATION_REDIS_ENDPOINT", "http://foo.bar")
	conf := Config{}
	assert.NoError(t, envconfig.Init(&conf))
}
