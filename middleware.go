package main

import (
	authtypes "bitbucket.org/to-increase/go-auth/types"
	"bitbucket.org/to-increase/go-sdk/services/machines/interface"
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/kpi-oee-api/repository"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/sirupsen/logrus"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
	"time"
)

type oeeContextKey string

const OEEConfigContextKey = oeeContextKey("kpi-config")

// GetOEEConfigFromContext returns the OEE config from the current context
// this function panics if no config is set in context
func GetOEEConfigFromContext(c context.Context) *types.Config {
	return c.Value(OEEConfigContextKey).(*types.Config)
}

// Reads the OEE config from database and store it in the context
func OEEConfigCtx(repo repository.Repository) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			mid := chi.URLParam(r, machineIDParam)
			config, err := repo.GetConfig(r.Context(), mid)
			if err != nil {
				logrus.Errorf("could not get the config for %s; %+v", mid, err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
			if config == nil {
				logrus.Warningf("machine %s does not have a kpi config", mid)
				http.Error(w, "oee config not found", http.StatusNotFound)
				return
			}

			ctx := context.WithValue(r.Context(), OEEConfigContextKey, config)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

// Reads the OEE config from database and store it in the context
func MachineAccess(machsvc machiniface.MachineAPI) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			mid := chi.URLParam(r, machineIDParam)
			mtrn, err := trn.Parse(mid)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			claims := authtypes.FromContext(r.Context())
			if claims.CompanyID() != mtrn.CompanyID {
				shared, err := machsvc.CheckMachineAccess(r.Context(), claims.CompanyID(), mid)
				if err != nil {
					logrus.Errorf("could not check shared machine %v; %+v", mid, err)
					http.Error(w, "", http.StatusInternalServerError)
					return
				}
				if !shared {
					http.Error(w, "you don't have access to this machine", http.StatusForbidden)
					return
				}
			}
			next.ServeHTTP(w, r)
		})
	}
}

type tsContextKey string

const timestampContextKey = tsContextKey("timestamps")

// GetTimestampsFromContext returns the query timestamps from the current context
func GetTimestampsFromContext(c context.Context) *types.Timeframe {
	return c.Value(timestampContextKey).(*types.Timeframe)
}

func Timestamp(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		dateFrom, err := strconv.ParseInt(chi.URLParam(r, "date_from"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		dateTo, err := strconv.ParseInt(chi.URLParam(r, "date_to"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if dateFrom > dateTo {
			http.Error(w, "date_to should be greater than date_from", http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), timestampContextKey, &types.Timeframe{DateFrom: startOfHour(dateFrom), DateTo: dateTo})

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func startOfHour(timestamp int64) int64 {
	return time.Unix(timestamp, 0).UTC().Truncate(time.Hour).Unix()
}
