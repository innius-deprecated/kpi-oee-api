package repository

import (
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"bitbucket.org/to-increase/vogels"
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
)

type Repository interface {
	SaveConfig(c context.Context, cfg types.Config) error
	UpdateLastUpdatedTimestamp(c context.Context, machineID string, timestamp int64) error
	GetConfig(c context.Context, machineID string) (*types.Config, error)
	DeleteConfig(c context.Context, machineID string) error
	GetAllConfigs(c context.Context) ([]*types.Config, error)
}

func NewRepository(db dynamodbiface.DynamoDBAPI, tableName string) Repository {
	return &repository{
		model: vogels.NewModelV2(db, &vogels.ModelDefinition{
			TableName: tableName,
			Key:       vogels.NewKey("id", vogels.StringType),
		}),
	}
}

type repository struct {
	model vogels.ModelAPIV2
}

func (r *repository) SaveConfig(c context.Context, cfg types.Config) error {
	_, err := r.model.PutItem(c, vogels.NewPutItemRequest(cfg))
	return errors.Wrap(err, "could not save oee config")
}

func (r *repository) UpdateLastUpdatedTimestamp(c context.Context, machineID string, timestamp int64) error {
	updated := types.Config{MachineID: machineID, LastUpdated:timestamp}
	ux, err := vogels.NewUpdateExpressionBuilder(updated).Field("last_updated").Build()
	if err != nil {
		return err
	}
	req := vogels.NewUpdateRequest(updated).WithUpdateExpression(ux)
	_, err = r.model.Update(c, req)
	return err
}

func (r *repository) GetConfig(c context.Context, machineID string) (*types.Config, error) {
	res := &types.Config{}
	found, err := r.model.GetItem(c, &res, machineID, nil)
	if err != nil {
		return nil, errors.Wrapf(err, "could not get oee config %s", machineID)
	}
	if found {
		return res, nil
	}
	return nil, nil
}

func (r *repository) DeleteConfig(c context.Context, machineID string) error {
	return errors.Wrapf(r.model.Destroy(c, machineID, nil), "could not delete oee config %s", machineID)
}

func (r *repository) GetAllConfigs(c context.Context) ([]*types.Config, error) {
	res := []*types.Config{}
	err := r.model.FullScan(c, &res)
	return res, err
}
