package repository

import (
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"testing"
)

func localDynamoDB() dynamodbiface.DynamoDBAPI {
	config := &aws.Config{}
	sess := session.New(config.WithEndpoint("http://localhost:8000").WithRegion("us-east-1"))
	return dynamodb.New(sess)
}

func TestRepository(t *testing.T) {
	ctx := context.Background()
	repo := NewRepository(localDynamoDB(), uuid.NewV4().String()).(*repository)
	repo.model.CreateTable(ctx)
	mtrn := trn.NewMachine(uuid.NewV4().String())

	config := types.Config{
		MachineID: mtrn.String(),
		StatusSensor: "foo",
	}

	if assert.NoError(t, repo.SaveConfig(ctx, config)) {
		cfg, err := repo.GetConfig(ctx, mtrn.String())
		if assert.NoError(t, err) {
			if assert.NotNil(t, cfg) {
				assert.Equal(t, &config, cfg)
			}
		}
		cfgs, err := repo.GetAllConfigs(ctx)
		if assert.NoError(t, err) {
			assert.Len(t, cfgs, 1)
		}
	}

	t.Run("update timestamp of last update", func(t *testing.T) {
		assert.NoError(t, repo.UpdateLastUpdatedTimestamp(ctx, mtrn.String(), 1234567))

		cfg, err := repo.GetConfig(ctx, mtrn.String())
		assert.NoError(t, err)
		assert.EqualValues(t, 1234567, cfg.LastUpdated)
		assert.NotEmpty(t, cfg.StatusSensor)
	})

	t.Run("delete the config", func(t *testing.T) {
		assert.NoError(t, repo.DeleteConfig(ctx, mtrn.String()))
		cfg, err := repo.GetConfig(ctx, mtrn.String())
		if assert.NoError(t, err) {
			assert.Nil(t, cfg)
		}
	})
}

