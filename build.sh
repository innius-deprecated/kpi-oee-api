#!/bin/bash
set -e

cat ./swagger.json | sed 's/{AWS::Region}/us-east-1/g; s/function:kpi-oee-api/function:chair-kpi-oee-api/g' > infrastructure/apigateway_swagger.json

go test -v ./...

name="chair-kpi-oee-api"

region=us-east-1;

GOOS=linux go build -o main && zip infrastructure/deployment.zip main

aws cloudformation package --template-file infrastructure/template.yml --s3-bucket artifacts.innius.$region --s3-prefix $name --output-template-file parsed-template.yml  --region $region

aws cloudformation deploy --template-file parsed-template.yml --stack-name $name --capabilities CAPABILITY_IAM --region $region \
 --parameter-overrides KpiEventSNSTopic=/chair/kpi-service/events_sns_topic MachineEventSNSTopic=/chair/machine-service/events_sns_topic AggregatedValuesRedisEndpoint=/chair/redis_endpoint Tag=latest

# aggregation-task

cd aggregation-batch

/bin/bash -c 'CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .'

$(aws ecr get-login --no-include-email --region us-east-1)

name="kpi-oee-aggregation"

docker build -t $name:latest .

docker tag ${name}:latest 831844703282.dkr.ecr.us-east-1.amazonaws.com/${name}:latest

docker push 831844703282.dkr.ecr.us-east-1.amazonaws.com/${name}:latest
