package main

import (
	. "bitbucket.org/innius/apihttp"
	"bitbucket.org/to-increase/go-sns/events/kpis"
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/calculation"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"

	"time"
)

// ErrResponse renderer type for handling all sorts of errors.
//
// In the best case scenario, the excellent github.com/pkg/errors package
// helps reveal information on the error, setting it on Err, and in the Render()
// method, using it to set the application-specific error code in AppCode.
// swagger:model errorResponse
type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

func ErrInternalServerError(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusInternalServerError,
		StatusText:     "internal server error",
	}
}

// PostConfigRequest  is the external representation of an innius oee kpi configuration
//
// An innius oee kpi configuration
//
// swagger:model PostConfigRequest
type PostConfigRequest struct {
	*types.Config
}

func (req *PostConfigRequest) Bind(r *http.Request) error {
	mid := chi.URLParam(r, machineIDParam)
	if _, err := trn.Parse(mid); err != nil {
		return err
	}
	req.MachineID = mid
	return nil
}

func newGetConfigResponse(c types.Config) *types.GetConfigResponse {
	return &types.GetConfigResponse{
		Config: &c,
	}
}

// swagger:operation POST /{machine_id}/config config postConfig
//
// post kpi
//
// Define the configuration for a machine OEE KPI
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: body
//   in: body
//   description: the oee kpi configuration
//   schema:
//     "$ref": "#/definitions/PostConfigRequest"
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the config has been posted
// security:
// - sigv4: []
func (srv *server) postConfig() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := PostConfigRequest{}
		if err := render.Bind(r, &req); err != nil {
			render.Render(w, r, ErrInvalidRequest(err))
			return
		}
		cfg := *req.Config
		existing, err := srv.repository.GetConfig(r.Context(), req.MachineID)
		if err != nil {
			render.Render(w, r, ErrInternalServerError(err))
		}
		if existing != nil && existing.CreatedAt > 0 {
			cfg.CreatedAt = existing.CreatedAt
		} else {
			cfg.CreatedAt = time.Now().Unix()
		}

		if err := srv.repository.SaveConfig(r.Context(), cfg); err != nil {
			render.Render(w, r, ErrInternalServerError(err))
			return
		}
		if existing == nil {
			if err := srv.kpiEventPublisher.Publish(r.Context(), kpis.KpiCreatedEvent{MachineID: cfg.MachineID, KPI: "OEE"}); err != nil {
				log.Errorf("Error posting KPI created event: %+v", err)
			}
		}
		w.WriteHeader(http.StatusOK)
	}
}

// swagger:operation GET /{machine_id}/config config getConfig
//
// get config
//
// Get the configuration for a machine OEE KPI
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the oee config
//     schema:
//       "$ref": "#/definitions/GetConfigResponse"
// security:
// - sigv4: []
func (srv *server) getConfig() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mid := chi.URLParam(r, machineIDParam)

		config, err := srv.repository.GetConfig(r.Context(), mid)
		if err != nil {
			logrus.Errorf("Error obtaining config: %+v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if config == nil {
			http.Error(w, "", http.StatusNotFound)
			return
		}
		render.Render(w, r, newGetConfigResponse(*config))
	}
}

// swagger:operation GET /{machine_id}/{date_from}/{date_to} getOEE calculate
//
// get oee
//
// Returns the OEE value and sub-KPIs for the given machine for a custom period (UTC)
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: date_from
//   in: path
//   description: the start timestamp for the calculation
//   type: string
//   required: true
// - name: date_to
//   in: path
//   description: the end timestamp for the calculation
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the oee values for the specified period
//     schema:
//       "$ref": "#/definitions/OeeResponse"
// security:
// - sigv4: []
func (srv *server) calculate(w http.ResponseWriter, r *http.Request) {
	config := GetOEEConfigFromContext(r.Context())
	mid := config.MachineID

	timeframe := GetTimestampsFromContext(r.Context())

	resp, err := calculateOEE(r.Context(), srv.query, mid, timeframe)
	if err != nil {
		log.Printf("calculation oee error %+v", err)
		http.Error(w, "Error calculating OEE", http.StatusInternalServerError)
		return
	}
	resp.Status = calculation.Status(resp.OEE, config.Thresholds)
	render.Render(w, r, resp)
}

func calculateOEE(ctx context.Context, query aggregation_repo.AggregationRepository, mid string, timeframe *types.Timeframe) (types.OeeResponse, error) {
	values, err := calculation.Calculate(ctx, query, mid, timeframe)
	if err != nil {
		return types.OeeResponse{}, err
	}
	previousTimes := previousTimestamps(timeframe)
	previousValues, err := calculation.Calculate(ctx, query, mid, previousTimes)
	if err != nil {
		logrus.Errorf("Error obtaining yesterdays OEE: %+v", err)
	}

	return types.OeeResponse{
		MachineID: mid,
		OeeValue:  values,
		TrendType: calculation.Trend(previousValues, values),
		FromDate:  timeframe.DateFrom,
		ToDate:    timeframe.DateTo,
	}, nil
}

// swagger:operation GET /{machine_id}/shift/{shift_id}/{date_from}/{date_to} getOEE calculateShift
//
// calculate shift oee
//
// Returns the OEE value and sub-KPIs for the given machine / shift for a custom period (UTC)
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: shift_id
//   in: path
//   description: the shift for which the oee must be recalculated
//   type: string
//   required: true
// - name: date_from
//   in: path
//   description: the start timestamp for the calculation
//   type: string
//   required: true
// - name: date_to
//   in: path
//   description: the end timestamp for the calculation
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the oee values for the specified period
//     schema:
//       "$ref": "#/definitions/OeeResponse"
// security:
// - sigv4: []
func (srv *server) calculateShift(w http.ResponseWriter, r *http.Request) {
	config := GetOEEConfigFromContext(r.Context())
	mid := config.MachineID

	timeframe := GetTimestampsFromContext(r.Context())

	shift := chi.URLParam(r, "shift_id")
	resp, err := calculateShiftOEE(r.Context(), srv.query, mid, shift, timeframe)
	if err != nil {
		log.Printf("calculation oee shift error %+v", err)
		http.Error(w, "Error calculating OEE for shift", http.StatusInternalServerError)
		return
	}
	resp.Status = calculation.Status(resp.OEE, config.Thresholds)

	render.Render(w, r, resp)
}

func calculateShiftOEE(ctx context.Context, query aggregation_repo.AggregationRepository, mid, shift string, timeframe *types.Timeframe) (types.OeeResponse, error) {
	values, err := calculation.CalculateShift(ctx, query, mid, shift, timeframe)
	if err != nil {
		return types.OeeResponse{}, err
	}
	previousTimes := previousTimestamps(timeframe)
	previousValues, err := calculation.CalculateShift(ctx, query, mid, shift, previousTimes)
	if err != nil {
		logrus.Errorf("Error obtaining yesterdays OEE: %+v", err)
	}

	return types.OeeResponse{
		MachineID: mid,
		OeeValue:  values,
		TrendType: calculation.Trend(previousValues, values),
		FromDate:  timeframe.DateFrom,
		ToDate:    timeframe.DateTo,
	}, nil
}

func previousTimestamps(tf *types.Timeframe) *types.Timeframe {
	from := tf.DateFrom - (tf.DateTo - tf.DateFrom)
	return &types.Timeframe{DateFrom: from, DateTo: tf.DateFrom}
}

// swagger:operation GET /{machine_id}/details/{date_from}/{date_to} getOEE calculateOEEDetails
//
// calculate oee details
//
// Returns the OEE value and sub-KPIs for the given machine for a custom period (UTC) in daily values
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
// - name: date_from
//   in: path
//   description: the start timestamp for the calculation
//   type: string
//   required: true
// - name: date_to
//   in: path
//   description: the end timestamp for the calculation
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the oee values for the specified period
//     schema:
//       type: array
//       items:
//         "$ref": "#/definitions/OeeDetailsResponse"
// security:
// - sigv4: []
func (srv *server) calculateDetails(w http.ResponseWriter, r *http.Request) {
	config := GetOEEConfigFromContext(r.Context())
	mid := config.MachineID

	timeframe := GetTimestampsFromContext(r.Context())

	resp, err := calculateOEEDetails(r.Context(), srv.query, mid, timeframe)
	if err != nil {
		log.Printf("calculation oee details error %+v", err)
		http.Error(w, "Error calculating OEE details", http.StatusInternalServerError)
		return
	}
	render.Render(w, r, resp)
}

func calculateOEEDetails(ctx context.Context, query aggregation_repo.AggregationRepository, mid string, timeframe *types.Timeframe) (types.OeeDetailsResponse, error) {
	detailedValues, totals, err := calculation.CalculateDetails(ctx, query, mid, timeframe)
	return types.OeeDetailsResponse{
		Values:   detailedValues,
		Totals:   totals,
		FromDate: timeframe.DateFrom,
		ToDate:   timeframe.DateTo,
	}, err
}

// swagger:operation PUT /{machine_id}/refresh oee refreshKPI
//
// refresh oee
//
// Refreshes the OEE
//
// ---
//
// parameters:
// - name: machine_id
//   in: path
//   description: the innius machine id (trn)
//   type: string
//   required: true
//
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the calculation is started
// security:
// - sigv4: []

// swagger:operation OPTIONS /{machine_id}/refresh oee
//
// ---
//
// responses:
//   '200':
//     description: "ok"
//     headers:
//       Access-Control-Allow-Origin:
//         type: "string"
//       Access-Control-Allow-Methods:
//         type: "string"
//       Access-Control-Allow-Headers:
//         type: "string"
// x-amazon-apigateway-integration:
//   responses:
//     default:
//       statusCode: "200"
//       description: ""
//       responseParameters:
//         method.response.header.Access-Control-Allow-Methods: "'PUT'"
//         method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
//         method.response.header.Access-Control-Allow-Origin: "'*'"
//   passthroughBehavior: "when_no_match"
//   requestTemplates:
//     application/json: "{\"statusCode\": 200}"
//   type: "mock"
func (srv *server) refreshNow(w http.ResponseWriter, r *http.Request) error {
	cfg := GetOEEConfigFromContext(r.Context())

	starttime := time.Now().Unix()

	timestring := r.URL.Query().Get("timestamp")
	if timestring != "" {
		starttime, _ = strconv.ParseInt(timestring, 10, 64)
	}

	end := time.Now().Unix()

	for ts := types.StartOfDay(starttime); ts < end; ts += 86400 {
		endtime := ts + 86400
		if endtime > end {
			endtime = end
		}
		log.Infof("aggregate oee for %+v [%d - %d]", cfg, ts, endtime)
		_, err := srv.aggregator.AggregateMachine(r.Context(), cfg, ts, endtime, true)
		if err != nil {
			return InternalServerError(err)
		}
	}
	if err := srv.repository.UpdateLastUpdatedTimestamp(r.Context(), cfg.MachineID, end); err != nil {
		return InternalServerError(err)
	}

	return NoContent(w, http.StatusOK)
}
