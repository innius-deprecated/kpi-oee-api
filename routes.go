package main

import (
	. "bitbucket.org/innius/apihttp"
	"bitbucket.org/to-increase/go-middleware/xray"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"net/http"
)

func stripPrefix(prefix string) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.StripPrefix(prefix, h)
	}
}

const (
	machineIDParam = "machine_id"
)

func (srv *server) routes() {
	r := chi.NewRouter()
	r.Use(xraymiddleware.XRayHandler(srv.config.EnvironmentName, srv.config.ServiceName))
	r.Use(stripPrefix(srv.config.BasePath))
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use()

	r.Post("/sns/machine_events", srv.machineEventHandler)

	r.Group(func(r chi.Router) {
		r.Route("/{"+machineIDParam+"}", func(r chi.Router) {
			r.Route("/config", func(r chi.Router) {
				r.Post("/", srv.postConfig())
				r.Get("/", srv.getConfig())
			})

			r.With(OEEConfigCtx(srv.repository)).Group(func(r chi.Router) {
				r.Put("/refresh", Handler(srv.refreshNow))

				r.With(Timestamp).Group(func(r chi.Router) {
					r.Get("/{date_from}/{date_to}", srv.calculate)
					r.Get("/shift/{shift_id}/{date_from}/{date_to}", srv.calculateShift)
					r.Get("/details/{date_from}/{date_to}", srv.calculateDetails)
				})
			})
		})
	})

	srv.Router = r
}
