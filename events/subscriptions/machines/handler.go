package machines

import (
	"bitbucket.org/to-increase/go-sns"
	"bitbucket.org/to-increase/go-sns/events/kpis"
	"bitbucket.org/to-increase/go-sns/events/machine"
	"bitbucket.org/to-increase/go-sns/subscriber"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/repository"
	"context"
	"github.com/aws/aws-xray-sdk-go/xray"
)

func New(r repository.Repository, a aggregation_repo.AggregationRepository, p publisher.Publisher) subscriber.EventHandler {
	s := &machineEventSubscription{
		MachineEventSubscription: &machineevents.MachineEventSubscription{},
		repo: r,
		aggregationRepo: a,
		kpiEventPublisher: p,
	}

	s.OnMachineDeleted(s.handleMachineDeletedEvent)
	s.OnCompanyDeleted(s.handleCompanyDeletedEvent)
	return s

}

// swagger:operation POST /sns/machine_events machineEvents machineEventsHandler
//
// ---
//
// parameters:
// x-amazon-apigateway-integration:
//   httpMethod: POST
//   type: aws_proxy
//   uri: arn:aws:apigateway:{AWS::Region}:lambda:path/2015-03-31/functions/arn:aws:lambda:{AWS::Region}:831844703282:function:kpi-oee-api/invocations
//   responses: {}
//
// responses:
//   '200':
//     description: the config has been posted
type machineEventSubscription struct {
	*machineevents.MachineEventSubscription
	repo repository.Repository
	aggregationRepo aggregation_repo.AggregationRepository
	kpiEventPublisher publisher.Publisher
}

func (s *machineEventSubscription) handleMachineDeletedEvent(c context.Context, evt machineevents.MachineDeletedEvent) error {
	return s.delete(c, evt.ID)
}

func (s *machineEventSubscription) handleCompanyDeletedEvent(c context.Context, evt machineevents.CompanyDeletedEvent) error {
	for _, m := range evt.Machines {
		s.delete(c, m)
	}
	return nil
}

func (s *machineEventSubscription) delete(c context.Context, mid string) error {
	if err := s.repo.DeleteConfig(c, mid); err != nil {
		return err
	}

	xray.CaptureAsync(c, "delete aggregated data", func(ctx context.Context) error {
		go func() {
			s.aggregationRepo.Delete(ctx, mid)
		}()
		return nil
	})
	return s.kpiEventPublisher.Publish(c, kpis.KpiDeletedEvent{MachineID:mid, KPI: "OEE"})
}