package types

type Timeframe struct {
	DateFrom int64
	DateTo int64
}

func (t *Timeframe) UseDailyAggregation() bool {
	return t.DateTo - t.DateFrom > 14 * 24 * 3600
}
