package types

import "encoding/json"

type TimestampedOEEAggregate struct {
	Timestamp     int64   `json:"timestamp"`
	OnTime        float64 `json:"on_time"`
	ErrorTime     float64 `json:"error_time"`
	TotalProduced float64 `json:"total_produced"`
	MaxProduced   float64 `json:"max_produced"`
	BadProduced   float64 `json:"bad_produced"`
}

func (s TimestampedOEEAggregate) MarshalBinary() ([]byte, error) {
	return json.Marshal(s)
}

func (s *TimestampedOEEAggregate) UnmarshalBinary(b []byte) error {
	return json.Unmarshal(b, s)
}

func (a *TimestampedOEEAggregate) HasData() bool {
	return a.OnTime > 0 || a.ErrorTime > 0
}

type TimestampedOEEAggregateWithShift struct {
	ShiftId string `json:"shift_id"`
	TimestampedOEEAggregate
}

type OeeValue struct {
	OEE          float64 `json:"oee"`
	Availability float64 `json:"availability"`
	Performance  float64 `json:"performance"`
	Quality      float64 `json:"quality"`

	OnTime        float64 `json:"on_time"`
	ErrorTime     float64 `json:"error_time"`
	TotalProduced float64 `json:"total_produced"`
	MaxProduced   float64 `json:"max_produced"`
	BadProduced   float64 `json:"bad_produced"`
}

type TimestampedOeeValue struct {
	OeeValue
	Timestamp int64 `json:"timestamp"`
}
