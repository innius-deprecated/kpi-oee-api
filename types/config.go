package types

import "time"

type Config struct {
	MachineID          string      `json:"id,omitempty"`
	StatusSensor       string      `json:"status_sensor"`
	ProducedRateSensor string      `json:"produced_rate_sensor"`
	MaxRateSensor      string      `json:"max_rate_sensor"`
	MaxValueFixed      float64     `json:"max_value_fixed"`
	BadRateSensor      string      `json:"bad_rate_sensor"`
	Thresholds         []Threshold `json:"thresholds"`
	CreatedAt          int64       `json:"created_at"`
	LastUpdated        int64       `json:"last_updated"` //timestamp of last aggregation

	Migrated bool `json:"migrated"` // flag which enables auto migration
}

type Threshold struct {
	//0 = normal, 1 = caution, 2 = emergency
	Status int     `json:"status"`
	Value  float64 `json:"value"`
}

func (c *Config) StartCalculationAt() int64 {
	if c.LastUpdated > c.CreatedAt {
		return c.LastUpdated
	}
	return c.CreatedAt
}
func StartOfDay(timestamp int64) int64 {
	return time.Unix(timestamp, 0).UTC().Truncate(24 * time.Hour).Unix()
}

func StartOfHour(timestamp int64) int64 {
	return time.Unix(timestamp, 0).UTC().Truncate(time.Hour).Unix()
}
