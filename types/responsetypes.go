package types

import "net/http"

// GetConfigResponse describes the response of the GetConfig operation
//
// An innius oee kpi configuration
//
// swagger:model GetConfigResponse
type GetConfigResponse struct {
	*Config
}

func (res *GetConfigResponse) Render(w http.ResponseWriter, r *http.Request) error {
	res.MachineID = ""
	return nil
}

// OeeResponse describes the response of GetKpi operations
//
// An innius oee value
//
// swagger:model OeeResponse
type OeeResponse struct {
	OeeValue
	MachineID string `json:"machine_id"`
	FromDate  int64  `json:"from_date"`
	ToDate    int64  `json:"to_date"`
	Status    int    `json:"status"`
	TrendType int    `json:"trend_type"`
}

func (res OeeResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// OeeDetailsResponse describes the response of GetKpiDetails operations
//
// An innius oee timeseries
//
// swagger:model OeeDetailsResponse
type OeeDetailsResponse struct {
	Values   []TimestampedOeeValue `json:"values"`
	Totals   OeeValue              `json:"totals"` //only here to not break frontend, please remove ASAP
	FromDate int64                 `json:"from_date"`
	ToDate   int64                 `json:"to_date"`
}

func (res OeeDetailsResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
