package main

import (
	"bitbucket.org/to-increase/go-auth/claims.v2"
	"bitbucket.org/to-increase/go-sns"
	"bitbucket.org/to-increase/go-trn"
	"bitbucket.org/to-increase/go-trn/uuid"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	authtypes "bitbucket.org/to-increase/go-auth/types"
	"bitbucket.org/to-increase/go-sdk/services/machines/interface"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/repository"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"bytes"
	"encoding/json"
	"golang.org/x/net/context"
)

type mockMachineSvc struct {
	machiniface.MachineAPI
	shared bool
}

func (s *mockMachineSvc) CheckMachineAccess(context.Context, string, string) (bool, error) {
	return s.shared, nil
}

type testserver struct {
	*server
	machsvc   *mockMachineSvc
	companyID string
}

func testJwtHandler(clms *claims.Claims) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r.WithContext(authtypes.ToContext(r.Context(), clms)))
		})
	}
}

type mockRepo struct {
	repository.Repository
	db map[string]*types.Config
}

func newMockRepo() *mockRepo {
	return &mockRepo{db: map[string]*types.Config{}}
}

func (r *mockRepo) SaveConfig(c context.Context, config types.Config) error {
	r.db[config.MachineID] = &config
	return nil
}

func (r *mockRepo) GetConfig(c context.Context, machineID string) (*types.Config, error) {
	return r.db[machineID], nil
}

func (r *mockRepo) DeleteConfig(c context.Context, machineID string) error {
	delete(r.db, machineID)
	return nil
}

type mockPub struct{}

func (m mockPub) Publish(context.Context, interface{}) error {
	return nil
}

func newMockPublisher() publisher.Publisher {
	return mockPub{}
}

func (t *testserver) CompanyID() string {
	return t.companyID
}

func newTestServer(cid string) *testserver {
	machsvc := &mockMachineSvc{}
	repo, _ := aggregation_repo.NewTestAggregationRepo()

	srv := &testserver{
		server: &server{
			repository:        newMockRepo(),
			machsvc:           machsvc,
			query:             repo,
			kpiEventPublisher: newMockPublisher(),
		},
		machsvc:   machsvc,
		companyID: cid,
	}

	srv.routes()

	return srv
}

func (srv *testserver) post(t *testing.T, path string, body io.Reader, expected int) (bool, *httptest.ResponseRecorder) {
	req, _ := http.NewRequest("POST", path, body)
	w := httptest.NewRecorder()

	srv.ServeHTTP(w, req)
	return assert.Equal(t, expected, w.Code), w
}

func (srv *testserver) get(t *testing.T, path string, expected int) (bool, *httptest.ResponseRecorder) {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil || req == nil {
		t.FailNow()
	}
	w := httptest.NewRecorder()

	srv.ServeHTTP(w, req)
	return assert.Equal(t, expected, w.Code), w
}

func (srv *testserver) delete(t *testing.T, path string, expected int) (bool, *httptest.ResponseRecorder) {
	req, _ := http.NewRequest("DELETE", path, nil)
	w := httptest.NewRecorder()

	srv.ServeHTTP(w, req)
	return assert.Equal(t, expected, w.Code), w
}

func TestPostConfig(t *testing.T) {
	srv := newTestServer(uuid.NewV4().String())
	mtrn := trn.NewMachine(srv.CompanyID())
	path := fmt.Sprintf("/%s/config", mtrn)

	req := PostConfigRequest{
		Config: &types.Config{
			StatusSensor:       "status_sensor",
			ProducedRateSensor: "produced_rate_sensor",
			BadRateSensor:      "bad__rate_sensor",
			MaxRateSensor:      "max_rate_sensor",
		},
	}
	var b bytes.Buffer
	if assert.NoError(t, json.NewEncoder(&b).Encode(&req)) {
		if ok, _ := srv.post(t, path, &b, http.StatusOK); !ok {
			t.FailNow()
		}
	}
	t.Run("get the config", func(t *testing.T) {
		srv.get(t, path, http.StatusOK)
	})
}

func TestCalculateOEE(t *testing.T) {
	srv := newTestServer(uuid.NewV4().String())
	mtrn := trn.NewMachine(srv.CompanyID()).String()

	assert.NoError(t, srv.repository.SaveConfig(context.Background(), types.Config{
		MachineID:          mtrn,
		StatusSensor:       "status_sensor",
		ProducedRateSensor: "produced_rate_sensor",
		BadRateSensor:      "bad__rate_sensor",
		MaxRateSensor:      "max_rate_sensor",
	}))

	day := int64(86400)

	t.Run("Post some values", func(t *testing.T) {
		ctx := context.Background()

		for i := int64(0); i < 7; i++ {
			agg := types.TimestampedOEEAggregate{
				Timestamp:     i * day,
				TotalProduced: 90,
				MaxProduced:   100,
				OnTime:        1,
			}
			srv.query.Set(ctx, mtrn, agg)
		}
	})

	t.Run("Get OEE", func(t *testing.T) {
		path := fmt.Sprintf("/%s/%d/%d", mtrn, 1*day, 22*day)

		ok, rec := srv.get(t, path, http.StatusOK)
		if ok {
			resp := types.OeeResponse{}
			assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
			assert.EqualValues(t, 90, resp.Performance)
			assert.EqualValues(t, 90, resp.OEE)
		}
		t.Run("Incorrect timestamps", func(t *testing.T) {
			path := fmt.Sprintf("/%s/%d/%d", mtrn, 8*day, 1*day)
			srv.get(t, path, http.StatusBadRequest)
			path = fmt.Sprintf("/%s/%v/%v", mtrn, "2018-01-01", "2019-01-01")
			srv.get(t, path, http.StatusBadRequest)
		})
	})

	t.Run("Get OEE for a shift", func(t *testing.T) {
		shiftId := uuid.NewV4().String()
		agg := types.TimestampedOEEAggregate{

			TotalProduced: 80,
			MaxProduced:   100,
			OnTime:        1,
			Timestamp:     1 * day,
		}

		srv.query.SetShift(context.Background(), mtrn, shiftId, agg)

		path := fmt.Sprintf("/%s/shift/%s/%d/%d", mtrn, shiftId, 1*day, 20*day)

		ok, rec := srv.get(t, path, http.StatusOK)
		if ok {
			resp := types.OeeResponse{}
			assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
			assert.EqualValues(t, 80, resp.Performance)
			assert.EqualValues(t, 80, resp.OEE)
		}
	})

	t.Run("Get OEE details", func(t *testing.T) {
		path := fmt.Sprintf("/%s/details/%d/%d", mtrn, 1*day, 19*day)

		ok, rec := srv.get(t, path, http.StatusOK)
		if ok {
			resp := types.OeeDetailsResponse{}
			assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &resp))
			assert.Len(t, resp.Values, 6)
			for _, v := range resp.Values {
				assert.EqualValues(t, 90, v.OEE)
			}
		}
	})
}
