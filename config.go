package main

type Config struct {
	LogLevel   string `envconfig:"default=info"`
	Repository struct {
		DynamoDB struct {
			TableName string
		}
	}
	Aggregation struct {
		Redis struct {
			Endpoint string
		}
	}
	EnvironmentName string
	ServiceName     string
	BasePath        string

	//AggregateBatch struct {
	//	ContainerName  string
	//	TaskDefinition string
	//}

	MachineEventSNSTopic    string
	KpiEventSNSTopic        string
	ExternalServiceEndpoint string

	Redshift struct {
		SecretName string
	}
}
