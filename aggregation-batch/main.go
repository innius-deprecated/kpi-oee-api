package main

import (
	_ "bitbucket.org/to-increase/go-middleware/xray"
	"context"
	"fmt"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/vrischmann/envconfig"
	"log"
	"time"
)

type Options struct {
	Date      int64
	Timeout   time.Duration
	MachineID string
}

func main() {
	conf := Config{}
	if err := envconfig.Init(&conf); err != nil {
		panic(err)
	}

	ctx, seg := xray.BeginSegment(context.Background(), conf.ServiceName)
	seg.AddAnnotation("stack", conf.EnvironmentName)
	if conf.Timeout != 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, conf.Timeout)
		defer cancel()
		go func() {
			<-ctx.Done()
			fmt.Println(ctx.Err()) // prints "context deadline exceeded"
		}()
	}
	handler := newHandler(conf)

	err := handler.Handle(ctx, conf)
	if err != nil {
		log.Printf("Error in handler: %+v", err)
	}
	seg.Close(err)
}
