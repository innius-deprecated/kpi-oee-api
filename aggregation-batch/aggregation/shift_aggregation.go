package aggregation

import (
	"context"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/pkg/errors"
)

func aggregateShifts(c context.Context, mid string, ts int64, end int64, results []QueryResult, as AggregationSystem) error {
	shiftAggregates, err := as.AggregateWithShifts(c, mid, ts, end, results)
	if err != nil {
		return errors.Wrapf(err, "could not aggregate shift values for %+v", mid)
	}
	for _, shiftAggregate := range shiftAggregates {
		if err := as.StoreShift(c, mid, shiftAggregate); err != nil {
			xray.AddError(c, err)
			return errors.Wrapf(err, "could not store aggregated shift values for %+v", mid)
		}
	}
	return nil
}
