package aggregation

import (
	"bitbucket.org/to-increase/go-database-connection"
	"bitbucket.org/to-increase/go-database-connection/redshift"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"time"
)

const (
	RedshiftFormat = "2006-01-02 15:04:05"
)

type QuerySystem interface {
	Query(ctx context.Context, oee *types.Config, start int64, end int64) ([]QueryResult, error)
}

type querySystem struct {
	redshift *sqlx.DB
}

func NewQuerySystem(secretname string) (QuerySystem, error) {
	conn, err := redshift.New(secretname)
	if err != nil {
		return nil, err
	}
	rs, err := connection.Open(conn)
	return &querySystem{rs}, err
}

func (q *querySystem) Query(ctx context.Context, oee *types.Config, start int64, end int64) ([]QueryResult, error) {
	starttime := time.Unix(start, 0)
	params := OEEQueryParams{
		Trn:            oee.MachineID,
		Starttimeepoch: starttime.Unix(),
		Starttime:      starttime.Format(RedshiftFormat),
		Endtime:        time.Unix(end, 0).UTC().Format(RedshiftFormat),
		Status:         oee.StatusSensor,
		Produced:       oee.ProducedRateSensor,
		Maxrate:        oee.MaxRateSensor,
		MaxrateVal:     oee.MaxValueFixed,
		Badrate:        oee.BadRateSensor,
	}

	maxRateFixed := false

	if oee.MaxRateSensor == "" {
		maxRateFixed = true
	}

	query := queryBuilder(maxRateFixed)
	result := []QueryResult{}
	err := xray.Capture(ctx, "read from redshift", func(c context.Context) error {
		rows, err := q.redshift.NamedQueryContext(ctx, query, params)
		if err != nil {
			return err
		}

		for rows.Next() {
			row := QueryResult{}
			if err := rows.StructScan(&row); err != nil {
				return err
			}
			result = append(result, row)
		}
		return nil
	})
	return result, err
}

type OEEQueryParams struct {
	Starttime      string
	Starttimeepoch int64
	Endtime        string
	Trn            string
	Status         string
	Produced       string
	Maxrate        string
	MaxrateVal     float64
	Badrate        string
}

const oeeQueryInit = `
	WITH timebuckets AS (
		SELECT ( EXTRACT(epoch FROM DATEADD(second, 60 * ( row_number() OVER (ORDER BY true) - 1), :starttime) ) ) AS bucket_timestamp
		FROM numbers LIMIT 1440 ), 
	status AS ( 
		(SELECT DISTINCT EXTRACT(epoch FROM event_timestamp) - EXTRACT(epoch FROM event_timestamp) % 60 AS ts,
		LAST_VALUE(sensor_reading_value) OVER (PARTITION BY ts) AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :status AND
		event_timestamp BETWEEN :starttime AND :endtime)
		UNION
		(SELECT CAST(:starttimeepoch AS INTEGER) AS ts, sensor_reading_value AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :status AND
		event_timestamp < :starttime ORDER BY event_timestamp DESC LIMIT 1)
	), 
	produced AS ( 
		(SELECT DISTINCT EXTRACT(epoch FROM event_timestamp) - EXTRACT(epoch FROM event_timestamp) % 60 AS ts,
		LAST_VALUE(sensor_reading_value) OVER (PARTITION BY ts) AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :produced AND
		event_timestamp BETWEEN :starttime AND :endtime)
		UNION
		(SELECT CAST(:starttimeepoch AS INTEGER) AS ts, sensor_reading_value AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :produced AND
		event_timestamp < :starttime ORDER BY event_timestamp DESC LIMIT 1)
	), 
	badrate AS ( 
		(SELECT DISTINCT EXTRACT(epoch FROM event_timestamp) - EXTRACT(epoch FROM event_timestamp) % 60 AS ts,
		LAST_VALUE(sensor_reading_value) OVER (PARTITION BY ts) AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :badrate AND
		event_timestamp BETWEEN :starttime AND :endtime)
		UNION
		(SELECT CAST(:starttimeepoch AS INTEGER) AS ts, sensor_reading_value AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :badrate AND
		event_timestamp < :starttime ORDER BY event_timestamp DESC LIMIT 1)
	)`

const maxRateFixedQuery = `, 
	maxrate AS ( 
		SELECT CAST(:starttimeepoch AS INTEGER) AS ts, CAST(:maxrateval AS NUMERIC(15,5)) AS value
	), `

const maxRateQuery = `, 
	maxrate AS ( 
		(SELECT DISTINCT EXTRACT(epoch FROM event_timestamp) - EXTRACT(epoch FROM event_timestamp) % 60 AS ts,
		LAST_VALUE(sensor_reading_value) OVER (PARTITION BY ts) AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :maxrate AND
		event_timestamp BETWEEN :starttime AND :endtime)
		UNION
		(SELECT CAST(:starttimeepoch AS INTEGER) AS ts, sensor_reading_value AS value FROM f_sensor_log WHERE
		machine_id = :trn AND
		sensor_id = :maxrate AND
		event_timestamp < :starttime ORDER BY event_timestamp DESC LIMIT 1)
	), `

const queryFinish string = `
	partial_data as (SELECT timebuckets.bucket_timestamp, status.value as status, produced.value as produced, maxrate.value as maxrate, badrate.value as badrate from timebuckets
		LEFT OUTER JOIN status ON timebuckets.bucket_timestamp = status.ts
		LEFT OUTER JOIN produced ON timebuckets.bucket_timestamp = produced.ts
		LEFT OUTER JOIN maxrate ON timebuckets.bucket_timestamp = maxrate.ts
		LEFT OUTER JOIN badrate ON timebuckets.bucket_timestamp = badrate.ts
		ORDER BY timebuckets.bucket_timestamp)
	SELECT
		bucket_timestamp AS timestamp,
		nvl(status, LAG(status) IGNORE NULLS OVER (ORDER BY bucket_timestamp), 0) as status,
		nvl(produced, LAG(produced) IGNORE NULLS OVER (ORDER BY bucket_timestamp), 0 ) as produced,
		nvl(maxrate, LAG(maxrate) IGNORE NULLS OVER (ORDER BY bucket_timestamp), 0 ) as maxrate,
		nvl(badrate, LAG(badrate) IGNORE NULLS OVER (ORDER BY bucket_timestamp), 0 ) as badrate
		from partial_data ORDER BY bucket_timestamp
`

func queryBuilder(maxRateFixed bool) string {
	if maxRateFixed {
		return oeeQueryInit + maxRateFixedQuery + queryFinish
	} else {
		return oeeQueryInit + maxRateQuery + queryFinish
	}
}

type QueryResult struct {
	Timestamp   int64   `db:"timestamp"`
	Status      float64 `db:"status"`
	Produced    float64 `db:"produced"`
	MaxProduced float64 `db:"maxrate"`
	BadProduced float64 `db:"badrate"`
}
