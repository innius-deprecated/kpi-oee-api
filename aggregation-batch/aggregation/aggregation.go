package aggregation

import (
	"bitbucket.org/to-increase/go-sdk/services/analytics/management"
	"bitbucket.org/to-increase/go-sdk/services/analytics/management/interface"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"math"
)

type AggregationSystem interface {
	Aggregate(ctx context.Context, mid string, start int64, queryResult []QueryResult) types.TimestampedOEEAggregate
	AggregateHourly(ctx context.Context, mid string, start int64, end int64, queryResult []QueryResult) []types.TimestampedOEEAggregate
	AggregateWithShifts(ctx context.Context, machine string, start int64, end int64, results []QueryResult) ([]types.TimestampedOEEAggregateWithShift, error)
	Store(ctx context.Context, machine string, oee types.TimestampedOEEAggregate) error
	StoreHourly(ctx context.Context, machine string, oee []types.TimestampedOEEAggregate) error
	StoreShift(ctx context.Context, machine string, oee types.TimestampedOEEAggregateWithShift) error
}

type aggregationSystem struct {
	queryRepo aggregation_repo.AggregationRepository
	shifts    managementiface.AnalyticsManagementAPI
}

func NewAggregationSystem(queryRepo aggregation_repo.AggregationRepository) AggregationSystem {
	return &aggregationSystem{
		queryRepo: queryRepo,
		shifts:    analytics_management.New(),
	}
}

func (a *aggregationSystem) Aggregate(ctx context.Context, mid string, start int64, queryResult []QueryResult) types.TimestampedOEEAggregate {
	result := calculateOEEValues(queryResult)
	result.Timestamp = start
	return result
}

func calculateOEEValues(results []QueryResult) types.TimestampedOEEAggregate {
	oee := types.TimestampedOEEAggregate{}
	for _, r := range results {
		if r.Status == 1 {
			oee.OnTime += 1
			oee.TotalProduced += r.Produced
			oee.MaxProduced += r.MaxProduced
			oee.BadProduced += r.BadProduced
		}
		if r.Status > 1 {
			oee.ErrorTime += 1
		}
	}
	return oee
}

const Hour = 3600

func (a *aggregationSystem) AggregateHourly(ctx context.Context, mid string, start int64, end int64, queryResult []QueryResult) []types.TimestampedOEEAggregate {
	hourlimit := getHourLimit(start, end)

	hourlyAggregates := []types.TimestampedOEEAggregate{}
	for h := 0; h < hourlimit; h++ { //TODO param calculation frequency
		hourStart := start + int64(h*Hour)
		agg := aggregateOverTime(hourStart, hourStart+Hour, queryResult)
		agg.Timestamp = hourStart + Hour //store aggregate on timestamp of end of hour
		hourlyAggregates = append(hourlyAggregates, agg)
	}
	return hourlyAggregates
}

func aggregateOverTime(start, end int64, results []QueryResult) types.TimestampedOEEAggregate {
	oee := types.TimestampedOEEAggregate{}
	for _, r := range results {
		if r.Timestamp >= start && r.Timestamp < end {
			if r.Status == 1 {
				oee.OnTime += 1
				oee.TotalProduced += r.Produced
				oee.MaxProduced += r.MaxProduced
				oee.BadProduced += r.BadProduced
			}
			if r.Status > 1 {
				oee.ErrorTime += 1
			}
		}
	}
	return oee
}

func (a *aggregationSystem) AggregateWithShifts(ctx context.Context, machine string, start int64, end int64, results []QueryResult) ([]types.TimestampedOEEAggregateWithShift, error) {
	resp, err := a.shifts.GetAllTimeseries(ctx, &analytics_management.GetAllTimeseriesInput{
		TRN:      machine,
		DateFrom: start,
		DateTo:   end, //TODO param calculation frequency
	})
	if err != nil {
		return nil, err
	}

	result := []types.TimestampedOEEAggregateWithShift{}

	shifts := sortByShiftID(resp.Timeboxes)
	for shiftId, timeboxes := range shifts {
		values := aggregateOverShift(timeboxes, results)
		values.Timestamp = start
		result = append(result, types.TimestampedOEEAggregateWithShift{
			ShiftId:                 shiftId,
			TimestampedOEEAggregate: values,
		})
	}
	return result, nil
}

type Timebox struct {
	Start  int64 `json:"start"`
	Finish int64 `json:"finish"`
}

func sortByShiftID(timeboxes []analytics_management.NamedTimebox) map[string][]Timebox {
	shifts := map[string][]Timebox{}
	for _, tb := range timeboxes {
		shift, ok := shifts[tb.Id]
		if ok {
			shifts[tb.Id] = append(shift, Timebox{Start: tb.Start, Finish: tb.Finish})
		} else {
			shifts[tb.Id] = []Timebox{{Start: tb.Start, Finish: tb.Finish}}
		}
	}
	return shifts
}

func aggregateOverShift(timeboxes []Timebox, results []QueryResult) types.TimestampedOEEAggregate {
	oee := types.TimestampedOEEAggregate{}
	for _, r := range results {
		if inTimeboxes(r.Timestamp, timeboxes) {
			if r.Status == 1 {
				oee.OnTime += 1
				oee.TotalProduced += r.Produced
				oee.MaxProduced += r.MaxProduced
				oee.BadProduced += r.BadProduced
			}
			if r.Status > 1 {
				oee.ErrorTime += 1
			}
		}
	}
	return oee
}

func inTimeboxes(ts int64, timeboxes []Timebox) bool {
	for _, tb := range timeboxes {
		if ts >= tb.Start && ts <= tb.Finish {
			return true
		}
	}
	return false
}

func (a *aggregationSystem) Store(ctx context.Context, machine string, oee types.TimestampedOEEAggregate) error {
	return a.queryRepo.Set(ctx, machine, oee)
}

func (a *aggregationSystem) StoreHourly(ctx context.Context, machine string, oees []types.TimestampedOEEAggregate) error {
	return a.queryRepo.SetHourly(ctx, machine, oees...)
}

func (a *aggregationSystem) StoreShift(ctx context.Context, machine string, oee types.TimestampedOEEAggregateWithShift) error {
	return a.queryRepo.SetShift(ctx, machine, oee.ShiftId, oee.TimestampedOEEAggregate)
}

//Converts seconds to number of hours and rounds down
func getHourLimit(start int64, end int64) int {
	seconds := float64(60)
	minutes := float64(60)
	totalseconds := float64(end - start)

	hourlimitfloat := (totalseconds / (seconds * minutes))
	return int(math.Floor(hourlimitfloat))
}
