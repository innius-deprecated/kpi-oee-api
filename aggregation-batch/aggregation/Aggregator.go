package aggregation

import (
	"bitbucket.org/to-increase/kpi-oee-api/calculation"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/pkg/errors"

	"time"
)

type Aggregator struct {
	QuerySystem       QuerySystem
	AggregationSystem AggregationSystem
}

func (h *Aggregator) AggregateMachine(c context.Context, kpi *types.Config, start int64, end int64, shifts bool) (*types.OeeValue, error) {
	queryResults, err := h.QuerySystem.Query(c, kpi, start, end)
	if err != nil {
		return nil, errors.Wrapf(err, "could not aggregate values for %+v", kpi)
	}

	values := h.AggregationSystem.Aggregate(c, kpi.MachineID, start, queryResults)
	if !values.HasData() {
		return nil, nil
	}

	if err := xray.Capture(c, "store aggregated values", func(c context.Context) error {
		return errors.Wrapf(h.AggregationSystem.Store(c, kpi.MachineID, values), "could not store aggregated values for %+v", kpi)
	}); err != nil {
		return nil, err
	}

	oee := calculation.ConvertToValues(values)

	err = xray.Capture(c, "run hourly aggregation", func(c context.Context) error {
		//run hourly aggregation
		hourlyAggregationStart := time.Unix(kpi.LastUpdated, 0).UTC().Truncate(time.Hour).Unix()
		if hourlyAggregationStart < start {
			hourlyAggregationStart = start
		}
		hourlyValues := h.AggregationSystem.AggregateHourly(c, kpi.MachineID, hourlyAggregationStart, end, queryResults)
		if err := h.AggregationSystem.StoreHourly(c, kpi.MachineID, hourlyValues); err != nil {
			return err
		}
		if shifts {
			return aggregateShifts(c, kpi.MachineID, start, end, queryResults, h.AggregationSystem)
		}
		return nil
	})
	return &oee, err
}
