package aggregation

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConversion(t *testing.T) {
	results := []QueryResult{
		{Timestamp: 1529932000, Status: 0, Produced: 0, MaxProduced: 100, BadProduced: 0},
		{Timestamp: 1529932060, Status: 1, Produced: 90, MaxProduced: 100, BadProduced: 9},
		{Timestamp: 1529932120, Status: 2, Produced: 10, MaxProduced: 100, BadProduced: 1},
		{Timestamp: 1529932180, Status: 4, Produced: 0, MaxProduced: 100, BadProduced: 0},
	}
	aggregate := calculateOEEValues(results)
	assert.EqualValues(t, 1, aggregate.OnTime)
	assert.EqualValues(t, 2, aggregate.ErrorTime)
	assert.EqualValues(t, 90, aggregate.TotalProduced)
	assert.EqualValues(t, 9, aggregate.BadProduced)
	assert.EqualValues(t, 100, aggregate.MaxProduced)
}
