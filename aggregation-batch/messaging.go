package main

import (
	kpievents "bitbucket.org/to-increase/go-sdk/services/kpis/events"
	"bitbucket.org/to-increase/go-event-queue"
	"github.com/pkg/errors"
	"context"
)

type Messaging struct {
	Mute bool
	Queue event_queue.DomainEventQueue
}

func (m Messaging) sendMessage(c context.Context, trn string, status int, value float64) error {
	if m.Mute {
		return nil
	}
	event := kpievents.ThresholdCrossedEvent{
		Machinetrn: trn,
		Kpi:        "OEE",
		Status:     status,
		Value:      value,
	}
	evt, err := event_queue.NewRawEventJson(kpievents.ThresholdCrossed, &event)
	if err != nil {
		return errors.Wrap(err, "Failed to convert event to json")
	}
	return m.Queue.PublishEventWithContext(c, evt)
}