package main

import (
	"bitbucket.org/to-increase/go-event-queue"
	. "bitbucket.org/to-increase/kpi-oee-api/aggregation-batch/aggregation"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/calculation"
	"bitbucket.org/to-increase/kpi-oee-api/repository"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/pkg/errors"
	"log"
	"time"
)

type handler struct {
	aggregator Aggregator
	cr         repository.Repository
	mess       Messaging
}

func newHandler(conf Config) handler {

	sess, err := session.NewSession()
	if err != nil {
		log.Printf("could not create aws session: %+v", err)
	}
	dyndb := dynamodb.New(sess)
	xray.AWS(dyndb.Client)
	configRepo := repository.NewRepository(dyndb, conf.Repository.DynamoDB.TableName)

	qurl := conf.Messaging.EventQueue

	qb := event_queue.NewDomainEventQueueBuilder().WithQueueUrl(qurl)

	messagingQueue, err := qb.WithSession(sess).Build()
	if err != nil {
		log.Printf("Error making messagingQueue: %+v", err)
	}
	mess := Messaging{Queue: messagingQueue, Mute: conf.Mute}

	queryRepo, err := aggregation_repo.NewAggregationRepo(conf.Aggregation.Redis.Endpoint)
	if err != nil {
		log.Fatal(err)
	}

	aggregationSystem := NewAggregationSystem(queryRepo)
	querySystem, err := NewQuerySystem(fmt.Sprintf("/%s/%s/redshift", conf.EnvironmentName, conf.ServiceName))
	if err != nil {
		log.Printf("Error connecting to database: %+v", err)
	}

	handler := handler{
		aggregator: Aggregator{QuerySystem: querySystem, AggregationSystem: aggregationSystem},
		cr:         configRepo,
		mess:       mess,
	}

	return handler
}

func (h *handler) Handle(c context.Context, config Config) error {
	configs, err := h.getKPIConfigs(c, h.cr, config.MachineID)
	if err != nil {
		return errors.Wrap(err, "error reading configs")
	}

	log.Printf("Running OEE aggregation for %v machines", len(configs))

	for i, kpi := range configs {
		log.Printf("Running machine %v, (%v / %v) ", kpi.MachineID, i, len(configs))
		starttime := kpi.StartCalculationAt()
		if config.CalculationDate > 0 {
			starttime = config.CalculationDate
		}

		if err := h.HandleMachine(c, kpi, types.StartOfDay(starttime), config.Shifts); err != nil {
			xray.AddError(c, err)
			log.Printf("%+v", err)
			continue
		}
		if err := h.cr.UpdateLastUpdatedTimestamp(c, kpi.MachineID, config.endDate()); err != nil {
			xray.AddError(c, err)
			log.Printf("could not update last aggregation timestamp for %+v; %+v", kpi, err)
		}
	}
	log.Printf("oee aggregation for %v-%v complete, worked %v configs", time.Unix(config.startDate(), 0).String(), time.Unix(config.endDate(), 0).String(), len(configs))
	return nil
}

func (h *handler) getKPIConfigs(c context.Context, configRepo repository.Repository, machineID string) ([]*types.Config, error) {
	if machineID != "" {
		kpi, err := configRepo.GetConfig(c, machineID)
		if err != nil {
			return nil, errors.Wrapf(err, "could not get config %+v", kpi)
		}
		if kpi == nil {
			return nil, errors.Wrapf(err, "no config defined for %s", machineID)
		}
		return []*types.Config{kpi}, nil
	} else {
		return configRepo.GetAllConfigs(c)
	}
}

func (h *handler) HandleMachine(c context.Context, kpiConfig *types.Config, starttime int64, shifts bool) error {
	end := time.Now().Unix()

	for ts := starttime; ts < end; ts += 86400 {
		endtime := ts + 86400
		if endtime > end {
			endtime = end
		}
		oee, err := h.aggregator.AggregateMachine(c, kpiConfig, ts, endtime, shifts)
		if err != nil {
			return err
		}
		if oee == nil {
			return nil
		}
		if status := calculation.Status(oee.OEE, kpiConfig.Thresholds); status > 0 {
			if err := h.mess.sendMessage(c, kpiConfig.MachineID, status, oee.OEE); err != nil {
				log.Printf("Error sending message %+v", err)
			}
		}
	}
	return nil
}
