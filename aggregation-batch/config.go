package main

import "time"

type Config struct {
	LogLevel   string `envconfig:"default=info"`
	Repository struct {
		DynamoDB struct {
			TableName string
		}
	}
	Aggregation struct {
		Redis struct {
			Endpoint string
		}
	}
	Messaging struct {
		EventQueue string
	}
	EnvironmentName string
	ServiceName     string

	MachineID       string        `envconfig:"optional"`
	CalculationDate int64         `envconfig:"default=-1"`
	Timeout         time.Duration `envconfig:"optional"`
	Shifts          bool          `envconfig:"default=true"`

	Mute bool `envconfig:"default=false"`
}

func (c Config) startDate() int64 {
	if c.CalculationDate >= 0 && c.CalculationDate <= time.Now().Unix() {
		return c.CalculationDate
	}
	return time.Now().Unix()
}

func (c Config) endDate() int64 {
	if c.CalculationDate >= 0 {
		return c.CalculationDate + 86400
	}
	return time.Now().Unix()
}
