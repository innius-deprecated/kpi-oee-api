package main

import (
	"bitbucket.org/to-increase/go-trn/uuid"
	. "bitbucket.org/to-increase/kpi-oee-api/aggregation-batch/aggregation"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	"bitbucket.org/to-increase/kpi-oee-api/types"
	"context"
	"github.com/aws/aws-xray-sdk-go/strategy/ctxmissing"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newTestQuerySystem() QuerySystem {
	return testQuerySystem{}
}

type testQuerySystem struct{}

func (t testQuerySystem) Query(ctx context.Context, oee *types.Config, start int64, end int64) ([]QueryResult, error) {
	return []QueryResult{{Status: 1, MaxProduced: 1000, Produced: 800, BadProduced: 200}}, nil
}

func TestUpdateTimestamp(t *testing.T) {
	xray.Configure(xray.Config{
		ContextMissingStrategy: ctxmissing.NewDefaultLogErrorStrategy(),
	})
	repo, err := aggregation_repo.NewTestAggregationRepo()
	assert.NoError(t, err)
	mid := uuid.NewV4().String()

	creationDate, _ := time.Parse(RedshiftFormat, "2018-01-01 12:34:56")
	queryDate, _ := time.Parse(RedshiftFormat, "2018-01-31 08:00:00")

	kpiConfig := &types.Config{
		MachineID: mid,
		CreatedAt: creationDate.Unix(),
	}

	handler := handler{
		aggregator: Aggregator{
			AggregationSystem: NewAggregationSystem(repo),
			QuerySystem:       newTestQuerySystem(),
		},
	}

	t.Run("Calculate the last 10 days", func(t *testing.T) {
		calcDate, _ := time.Parse(RedshiftFormat, "2018-01-21 12:34:56")

		assert.NoError(t, handler.HandleMachine(context.Background(), kpiConfig, calcDate.Unix(), false))
		dailyValues, err := repo.GetDetails(context.Background(), mid, &types.Timeframe{DateFrom: creationDate.Unix(), DateTo: queryDate.Unix()})
		assert.NoError(t, err)
		assert.Len(t, dailyValues, 10)
	})
}
