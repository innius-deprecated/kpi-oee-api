package main

import (
	"bitbucket.org/to-increase/go-sdk/services/machines"
	"bitbucket.org/to-increase/go-sdk/services/machines/interface"
	"bitbucket.org/to-increase/go-sns"
	"bitbucket.org/to-increase/go-sns/subscriber"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation-batch/aggregation"
	"bitbucket.org/to-increase/kpi-oee-api/aggregation_repo"
	machine_events "bitbucket.org/to-increase/kpi-oee-api/events/subscriptions/machines"
	"bitbucket.org/to-increase/kpi-oee-api/repository"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/aws/aws-sdk-go/service/ecs/ecsiface"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/go-chi/chi"
	"net/http"
)

type server struct {
	chi.Router
	config     Config
	repository repository.Repository
	query      aggregation_repo.AggregationRepository
	ecsClient  ecsiface.ECSAPI
	machsvc    machiniface.MachineAPI

	machineEventSubscr  subscriber.Subscriber
	machineEventHandler http.HandlerFunc
	kpiEventPublisher   publisher.Publisher

	aggregator aggregation.Aggregator
}

func NewServer(conf Config) (*server, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	db := dynamodb.New(sess)
	xray.AWS(db.Client)

	snsClient := sns.New(sess)

	subscr := subscriber.NewSubscriber(snsClient, conf.MachineEventSNSTopic, conf.ExternalServiceEndpoint+"/sns/machine_events")
	kpiPublisher := publisher.NewPublisher(snsClient, conf.KpiEventSNSTopic)

	repo := repository.NewRepository(db, conf.Repository.DynamoDB.TableName)
	aggregationRepo, err := aggregation_repo.NewAggregationRepo(conf.Aggregation.Redis.Endpoint)
	if err != nil {
		return nil, err
	}

	qs, err := aggregation.NewQuerySystem(conf.Redshift.SecretName)
	if err != nil {
		return nil, err
	}

	srv := &server{
		config:     conf,
		repository: repo,
		query:      aggregationRepo,
		ecsClient:  ecs.New(sess),
		machsvc:    machines.New(),

		machineEventSubscr:  subscr,
		machineEventHandler: subscr.HttpHandler(machine_events.New(repo, aggregationRepo, kpiPublisher)),
		kpiEventPublisher:   kpiPublisher,
		aggregator: aggregation.Aggregator{
			QuerySystem:       qs,
			AggregationSystem: aggregation.NewAggregationSystem(aggregationRepo),
		},
	}

	return srv, nil
}
